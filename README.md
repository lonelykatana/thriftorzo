Thriftorzo website :  https://thriftorzo.vercel.app

The techstack used for the backend of the website are Java 8, Spring boot, PostgreSQL, and Heroku for deployment.
The documentation for the APIs can be found here : https://thriftorzo-api.herokuapp.com/swagger-ui/index.html
